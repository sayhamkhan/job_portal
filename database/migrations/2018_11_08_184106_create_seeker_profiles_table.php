<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeekerProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seeker_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('father_name', 32)->nullable();
            $table->string('mother_name', 32)->nullable();
            $table->string('curent_location', 32)->nullable();
            $table->string('address', 32)->nullable();
            $table->string('nationality', 32)->nullable();
            $table->string('birth_date', 32)->nullable();
            $table->string('sek_categoory', 32)->nullable();
            $table->string('sek_deg', 32)->nullable();
            $table->string('marital_status', 32)->nullable();
            $table->string('gender', 32)->nullable();
            $table->string('sek_dec')->nullable();
            $table->string('profile_photo', 128)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('seeker_profiles');
    }
}
