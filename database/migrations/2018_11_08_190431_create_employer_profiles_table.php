<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployerProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employer_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('cmp_name', 32)->nullable();
            $table->string('cmp_tagline', 32)->nullable();
            $table->string('cmp_categoory', 32)->nullable();
            $table->string('cmp_city', 32)->nullable();
            $table->string('cnt_designation', 32)->nullable();
            $table->string('cmp_website', 32)->nullable();
            $table->string('cmp_address', 32)->nullable();
            $table->string('employe_rang', 32)->nullable();
            $table->string('cmp_logo')->nullable();
            $table->string('cmp_dec')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employer_profiles');
    }
}
