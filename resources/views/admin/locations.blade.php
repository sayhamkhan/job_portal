@extends('admin.master')

@section('contant')

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form action="{{route('add_location_process')}}" role="" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row ptb_20 bb_2">
                        <h3 class="head">Add Location:</h3>
                        <div class="col-md-12 col-sm-12 form-group">
                            <div class="input-group">
                                <input type="text" name="location_name" class="form-control" placeholder="Add Location" value="{{ old('location_name') }}">
                                @if($errors->has('location_name'))
                                <span class="label label-danger">
                                    {{ $errors->first('location_name') }}
                                </span>
                                @endif
                            </div>

                            <div class="col-md-12 col-sm-12 mt_10">
                                <button class="btn btn-success btn-primary add_btn">Add</button>    
                            </div> 
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="user_list_header">
                        <h4 class="title">Location List</h4>
                    </div>
                    <div class="content table-responsive table-full-width">
                        <table class="table table-hover table-striped">
                            <thead class="user_list_head">
                                <th class="single_user_head">Location Name</th>
                                <th class="single_user_head">Action</th>
                            </thead>
                            <tbody>
                                @foreach($locations_list as $locations_lists)
                                <tr>
                                    <td>{{$locations_lists->location_name}}</td>
                                    <td><a class="btn btn-primary edit_btn" href="{{route('edit_location', $locations_lists->id)}} " role="button">Edit</a>
                                    <form action="{{ route('delete_location_process', $locations_lists->id)}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger">
                                        Delete
                                    </button>
                                    </form></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
