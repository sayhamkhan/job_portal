@extends('admin.master')

@section('contant')

<section class="job_category bg_1 section_padding">
    <div class="row">
        <div class="col-md-9 all_cat admin_notification">
            <div class="row ptb_20 bb_2">
                <h2 class="detail-title">Message</h2>
                <div class="col-md-12 col-sm-6 form-group">
                    @foreach($notification_data_single as $notification_data_singles)
                    <div class="input-group">
                        <span class="title">
                            {{$notification_data_singles->job_title}}
                        </span>
                    </div>
                    <div class="input-group">
                        <span class="title">
                            {{$notification_data_singles->first_name}}
                            {{$notification_data_singles->last_name}}
                        </span>
                    </div>
                    <div class="input-group logo_fprm date_form">
                        <span class="title">
                            {{$notification_data_singles->message}}
                        </span>
                    </div>
                    <div class="input-group logo_fprm date_form">
                        <span class="title">
                            {{$notification_data_singles->sec_deg}}
                        </span>
                    </div>
                    <div class="input-group logo_fprm date_form">
                        <span class="title">
                            {{$notification_data_singles->exp_salary}}
                        </span>
                    </div>

                    <div class="input-group">
                        <span class="title">
                            {{$notification_data_singles->salary}}
                        </span>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endsection