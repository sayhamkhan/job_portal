@extends('admin.master')
@section('contant')

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form action="{{route('edit_location_process', $data->id)}}" role="" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="row ptb_20 bb_2">
                        <h3 class="head">Edit Location:</h3>
                        <div class="col-md-12 col-sm-12 form-group">
                            <div class="input-group">
                                <input type="text" name="location_name" class="form-control" placeholder="Add Location" value="{{$data->location_name }}">
                                @if($errors->has('location_name'))
                                <span class="label label-danger">
                                    {{ $errors->first('location_name') }}
                                </span>
                                @endif
                            </div>

                            <div class="col-md-12 col-sm-12 mt_10">
                                <button class="btn btn-success btn-primary add_btn">Update</button> 
                            </div> 
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
