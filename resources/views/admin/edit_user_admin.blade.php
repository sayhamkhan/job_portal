@extends('admin.master')
@section('contant')

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form action="{{route('admin_edit_user_process', $data->id)}}" role="" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row ptb_20 bb_2">
                        <h3 class="head">Edit User</h3>
                        <div class="col-md-12 col-sm-12 form-group">
                            <div class="input-group">
                                <input type="text" name="first_name" class="form-control" value="{{$data->first_name}}">
                                @if($errors->has('first_name'))
                                <span class="label label-danger">
                                    {{ $errors->first('first_name') }}
                                </span>
                                @endif
                            </div>
                            <div class="input-group">
                                <input type="text" name="last_name" class="form-control" value="{{$data->last_name}}">
                                @if($errors->has('last_name'))
                                <span class="label label-danger">
                                    {{ $errors->first('last_name') }}
                                </span>
                                @endif
                            </div>
                            <div class="input-group">
                                <input type="text" name="user_name" class="form-control" value="{{$data->user_name}}">
                                @if($errors->has('user_name'))
                                <span class="label label-danger">
                                    {{ $errors->first('user_name') }}
                                </span>
                                @endif
                            </div>
                            <div class="input-group">
                                <input type="text" name="phone" class="form-control" value="{{$data->phone}}">
                                @if($errors->has('phone'))
                                <span class="label label-danger">
                                    {{ $errors->first('phone') }}
                                </span>
                                @endif
                            </div>

                            <div class="col-md-12 col-sm-12 mt_10">
                                <button type="submit" class="btn btn-success btn-primary add_btn">Update</button> 
                            </div> 
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
