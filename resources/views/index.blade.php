@extends('master')

@section('slider')

    @include('partials.slider')

@stop

@section('contant')

<!--=============Feature Jobs Start Here=============-->
<section class="feature_job section_padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="all_feat_job">
                    <h1 class="feature_job_head">
                        Feature Jos
                    </h1>
                    <ul class="feat_all">
                        @foreach($feature_job as $feature_jobs)
                        <li class="sin_feat_job">
                            <a href="{{route('jobs')}}">
                                <span class="feat_job_con">
                                    <ul class="feat_job_con_all">
                                        <li class="feat_job_con_tit">{{$feature_jobs->job_title}}</li>
                                        <li class="feat_job_con_dec">{{$feature_jobs->location}}</li>
                                        <li class="feat_job_con_dec">{{$feature_jobs->salary}}</li>
                                        <li class="feat_job_con_dec">{{$feature_jobs->job_level}}</li>
                                    </ul>
                                </span>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!--=============Feature Jobs End Here=============-->

@stop

@section('footer_widgets')

    @include('partials.footer_widgets')

@endsection