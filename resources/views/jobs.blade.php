@extends('master')


@section('contant')

<!--=============Job_Category Start Here=============-->
<section class="job_category bg_1 section_padding">
    <div class="row">
        <div class="col-md-12 all_cat">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-hover table-striped" id="dataTables">
                        <thead>
                            <tr>
                                <th>Job Title</th>
                                <th>Job Category</th>
                                <th>Job Salary</th>
                                <th>Job Level</th>
                                <th>Job Type</th>
                                <th>Job Location</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($emp_job_data as $emp_job_datas)
                            <tr>
                                <td>
                                    <div class="brows-job-position">
                                        <a href="#"><h3>{{ $emp_job_datas->job_title }}</h3></a>
                                    </div>
                                </td>
                                <td>
                                    <div class="brows-job-position">
                                        <p>
                                            <span>{{ $emp_job_datas->job_cate }}</span>
                                        </p>
                                    </div>
                                </td>
                                <td>
                                    <div class="brows-job-position">
                                        <p>
                                            <span class="brows-job-sallery"><i class="far fa-money-bill-alt"></i>{{ $emp_job_datas->salary }} Taka</span>
                                        </p>
                                    </div>
                                </td>
                                <td>
                                    <div class="brows-job-position">
                                        <p>
                                            <span class="job-type cl-success bg-trans-success">{{ $emp_job_datas->job_level }}</span>
                                        </p>
                                    </div>
                                </td>
                                <td>
                                    <div class="brows-job-position">
                                        <p>
                                            <span class="job-type cl-success bg-trans-success">{{ $emp_job_datas->job_type }}</span>
                                        </p>
                                    </div>
                                </td>

                                <td>
                                    <div class="brows-job-location">
                                        <p><i class="fa fa-map-marker"></i>{{ $emp_job_datas->location }}</p>
                                    </div>
                                </td>
                                <td>
                                    @guest
                                    <div class="brows-job-link">
                                        <a href="{{route('login')}}" class="btn btn-success">Register/Login</a>
                                    </div>
                                    @else
                                    <div class="brows-job-link">
                                        <a href="{{route('job_details', $emp_job_datas->id)}}" class="btn btn-success">Details</a>
                                    </div>
                                    @endguest
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!--=============Job_Category End Here=============-->

@stop

@section('footer_widgets')

    @include('partials.footer_widgets')

@endsection