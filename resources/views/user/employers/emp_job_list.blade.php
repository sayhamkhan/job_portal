@extends('user.employers.master')

@section('contant')

<div class="row">
	<div class="col-md-12">
		@foreach($emp_job_data as $emp_job_datas)
		<div class="item-click">
			<article>
				<div class="brows-job-list">
					<div class="row">
						<div class="col-md-1 col-sm-2 small-padding">
							<div class="brows-job-company-img">
								<a href="#"><img src="img/ind1.jpg" class="img-responsive" alt="" /></a>
							</div>
						</div>
						<div class="col-md-5 col-sm-4">
							<div class="brows-job-position">
								<a href="#"><h3>{{ $emp_job_datas->job_title }}</h3></a>
								<p>
									<span>{{ $emp_job_datas->job_cate }}</span><span class="brows-job-sallery"><i class="far fa-money-bill-alt"></i>{{ $emp_job_datas->salary }} Taka</span>
									<span class="job-type cl-success bg-trans-success">{{ $emp_job_datas->job_level }}</span>
								</p>
							</div>
						</div>
						<div class="col-md-2 col-sm-2">
							<div class="brows-job-location">
								<p><i class="fa fa-map-marker"></i>{{ $emp_job_datas->location }}</p>
							</div>
						</div>
						<div class="col-md-4 col-sm-2">
							<div class="brows-job-link job_list_btn">
								<a href="{{route('application_list', $emp_job_datas->id)}}" class="btn btn-success">Applications</a>
							</div>
							<div class="brows-job-link job_list_btn">
								<a href="{{route('emp_job_details', $emp_job_datas->id)}}" class="btn btn-success">Details</a>
							</div>
						</div>
					</div>
				</div>
			</article>
		</div>
		@endforeach
	</div>
</div>

@endsection