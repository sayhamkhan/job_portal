@extends('user.employers.master')

@section('contant')

<div class="row">
	<div class="col-md-12">
		@foreach($application_data as $application_datas)
		<div class="item-click">
			<article>
				<div class="brows-job-list">
					<div class="row">
						<div class="col-md-6 col-sm-4">
							<div class="brows-job-position">
								<a href="#">
									<h3>
										{{ $application_datas->seeker_first_name }}
										{{ $application_datas->seeker_last_name }}
									</h3>
								</a>
								<p>
									<span>{{ $application_datas->seeker_deg }}</span>
									<span class="brows-job-sallery">{{ $application_datas->seeker_gender }}</span>
								</p>
							</div>
						</div>
						<div class="col-md-2 col-sm-2">
							<div class="brows-job-location">
								<p>Expacted Salary: {{ $application_datas->exp_salary }}</p>
							</div>
						</div>
						<div class="col-md-4 col-sm-2">
							<div class="brows-job-link app_list_btn job_list_btn">
								<a href="{{route('seeker_datils_by_job', $application_datas->sekker_id)}}" class="btn btn-success">Details</a>
							</div>
							@if($application_datas->status==0)
							<div class="brows-job-link app_list_btn job_list_btn">

								<button type="button" class="btn btn-success" data-toggle="modal" data-target="#status_zero_modal_one">
				                  Selected
				                </button>

				                <!-- Modal -->
				                <div class="modal fade" id="status_zero_modal_one" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
				                    <div class="modal-dialog modal-dialog-centered" role="document">
				                        <div class="modal-content">
				                            <div class="modal-header">
				                            <h5 class="modal-title" id="exampleModalLongTitle">Send Notification</h5>
				                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				                              <span aria-hidden="true">&times;</span>
				                            </button>
				                            </div>
				                            <div class="modal-body">
												<div class="modal_job_inp">
													<div class="modal_job_summ">
				                                       <span class="mjs_con">
				                                           {{ $application_datas->seeker_first_name }}
				                                           {{ $application_datas->seeker_last_name }}
				                                       </span><br>
				                                       <span class="mjs_con">
				                                           {{ $application_datas->seeker_deg }}
				                                       </span>
				                                   </div><br>
			                                        <form action="{{route('select_status_value', $application_datas->id)}}" role="" method="post">
			                                            {{csrf_field()}}
			                                            <div class="input-group form-group">
			                                                <label class="exp_level" for="exp_salary">Message</label><br>
			                                                <textarea class="form-control textarea" name="message" value="{{ old('message') }}" placeholder="About Job Seeker"></textarea>
			                                                @if($errors->has('message'))
			                                                    <span class="label label-danger">
			                                                        {{ $errors->first('message') }}
			                                                    </span>
			                                                @endif 
			                                            </div>
			                                            <div class="form-group">
			                                                <button type="Submit" class="btn btn-success">Submit</button>
			                                            </div>
			                                        </form>
		                                    	</div>
				                            </div>
				                            <div class="modal-footer">
				                            <button type="button" class="btn btn-primary modal_btn" data-dismiss="modal">Close</button>
				                            </div>
				                        </div>
				                    </div>
				                </div>

							</div>

							<div class="brows-job-link app_list_btn job_list_btn">

								<button type="button" class="btn btn-success sel_btn" data-toggle="modal" data-target="#status_zero_modal_two">
				                  Rejected
				                </button>

				                <!-- Modal -->
				                <div class="modal fade" id="status_zero_modal_two" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
				                    <div class="modal-dialog modal-dialog-centered" role="document">
				                        <div class="modal-content">
				                            <div class="modal-header">
				                            <h5 class="modal-title" id="exampleModalLongTitle">Send Notification</h5>
				                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				                              <span aria-hidden="true">&times;</span>
				                            </button>
				                            </div>
				                            <div class="modal-body">
												<div class="modal_job_inp">
													<div class="modal_job_summ">
				                                       <span class="mjs_con">
				                                           {{ $application_datas->seeker_first_name }}
				                                           {{ $application_datas->seeker_last_name }}
				                                       </span><br>
				                                       <span class="mjs_con">
				                                           {{ $application_datas->seeker_deg }}
				                                       </span>
				                                   </div><br>
			                                        <form action="{{route('reject_status_value', $application_datas->id)}}" role="" method="post">
			                                            {{csrf_field()}}
			                                            <div class="input-group form-group">
			                                                <label class="exp_level" for="exp_salary">Message</label><br>
			                                                <textarea class="form-control textarea" name="message" value="{{ old('message') }}" placeholder="About Job Seeker"></textarea>
			                                                @if($errors->has('message'))
			                                                    <span class="label label-danger">
			                                                        {{ $errors->first('message') }}
			                                                    </span>
			                                                @endif 
			                                            </div>
			                                            <div class="form-group">
			                                                <button type="Submit" class="btn btn-success">Submit</button>
			                                            </div>
			                                        </form>
		                                    	</div>
				                            </div>
				                            <div class="modal-footer">
				                            <button type="button" class="btn btn-primary modal_btn" data-dismiss="modal">Close</button>
				                            </div>
				                        </div>
				                    </div>
				                </div>

							</div>
							@elseif($application_datas->status==1)
							<div class="brows-job-link app_list_btn job_list_btn">
								<button type="button" class="btn btn-success sel_btn" data-toggle="modal" data-target="#status_one_modal_one">
				                  Selected
				                </button>

				                <!-- Modal -->
				                <div class="modal fade" id="status_one_modal_one" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
				                    <div class="modal-dialog modal-dialog-centered" role="document">
				                        <div class="modal-content">
				                            <div class="modal-header">
				                            <h5 class="modal-title" id="exampleModalLongTitle">Send Notification</h5>
				                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				                              <span aria-hidden="true">&times;</span>
				                            </button>
				                            </div>
				                            <div class="modal-body">
												<div class="modal_job_inp">
													<div class="modal_job_summ">
				                                       <span class="mjs_con">
				                                           {{ $application_datas->seeker_first_name }}
				                                           {{ $application_datas->seeker_last_name }}
				                                       </span><br>
				                                       <span class="mjs_con">
				                                           {{ $application_datas->seeker_deg }}
				                                       </span>
				                                   </div><br>
			                                        <form action="{{route('select_status_value', $application_datas->id)}}" role="" method="post">
			                                            {{csrf_field()}}
			                                            <div class="input-group form-group">
			                                                <label class="exp_level" for="exp_salary">Message</label><br>
			                                                <textarea class="form-control textarea" name="message" value="{{ old('message') }}" placeholder="About Job Seeker"></textarea>
			                                                @if($errors->has('message'))
			                                                    <span class="label label-danger">
			                                                        {{ $errors->first('message') }}
			                                                    </span>
			                                                @endif 
			                                            </div>
			                                            <div class="form-group">
			                                                <button type="Submit" class="btn btn-success">Submit</button>
			                                            </div>
			                                        </form>
		                                    	</div>
				                            </div>
				                            <div class="modal-footer">
				                            <button type="button" class="btn btn-primary modal_btn" data-dismiss="modal">Close</button>
				                            </div>
				                        </div>
				                    </div>
				                </div>

							</div>
							<div class="brows-job-link app_list_btn job_list_btn">
								<button type="button" class="btn btn-success" data-toggle="modal" data-target="#status_one_modal_two">
				                  Rejected
				                </button>

				                <!-- Modal -->
				                <div class="modal fade" id="status_one_modal_two" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
				                    <div class="modal-dialog modal-dialog-centered" role="document">
				                        <div class="modal-content">
				                            <div class="modal-header">
				                            <h5 class="modal-title" id="exampleModalLongTitle">Send Notification</h5>
				                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				                              <span aria-hidden="true">&times;</span>
				                            </button>
				                            </div>
				                            <div class="modal-body">
												<div class="modal_job_inp">
													<div class="modal_job_summ">
				                                       <span class="mjs_con">
				                                           {{ $application_datas->seeker_first_name }}
				                                           {{ $application_datas->seeker_last_name }}
				                                       </span><br>
				                                       <span class="mjs_con">
				                                           {{ $application_datas->seeker_deg }}
				                                       </span>
				                                   </div><br>
			                                        <form action="{{route('reject_status_value', $application_datas->id)}}" role="" method="post">
			                                            {{csrf_field()}}
			                                            <div class="input-group form-group">
			                                                <label class="exp_level" for="exp_salary">Message</label><br>
			                                                <textarea class="form-control textarea" name="message" value="{{ old('message') }}" placeholder="About Job Seeker"></textarea>
			                                                @if($errors->has('message'))
			                                                    <span class="label label-danger">
			                                                        {{ $errors->first('message') }}
			                                                    </span>
			                                                @endif 
			                                            </div>
			                                            <div class="form-group">
			                                                <button type="Submit" class="btn btn-success">Submit</button>
			                                            </div>
			                                        </form>
		                                    	</div>
				                            </div>
				                            <div class="modal-footer">
				                            <button type="button" class="btn btn-primary modal_btn" data-dismiss="modal">Close</button>
				                            </div>
				                        </div>
				                    </div>
				                </div>

							</div>
							@else
							<div class="brows-job-link app_list_btn job_list_btn">
								
								<button type="button" class="btn btn-success" data-toggle="modal" data-target="#status_two_modal_one">
				                  Selected
				                </button>

				                <!-- Modal -->
				                <div class="modal fade" id="status_two_modal_one" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
				                    <div class="modal-dialog modal-dialog-centered" role="document">
				                        <div class="modal-content">
				                            <div class="modal-header">
				                            <h5 class="modal-title" id="exampleModalLongTitle">Send Notification</h5>
				                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				                              <span aria-hidden="true">&times;</span>
				                            </button>
				                            </div>
				                            <div class="modal-body">
												<div class="modal_job_inp">
													<div class="modal_job_summ">
				                                       <span class="mjs_con">
				                                           {{ $application_datas->seeker_first_name }}
				                                           {{ $application_datas->seeker_last_name }}
				                                       </span><br>
				                                       <span class="mjs_con">
				                                           {{ $application_datas->seeker_deg }}
				                                       </span>
				                                   </div><br>
			                                        <form action="{{route('select_status_value', $application_datas->id)}}" role="" method="post">
			                                            {{csrf_field()}}
			                                            <div class="input-group form-group">
			                                                <label class="exp_level" for="exp_salary">Message</label><br>
			                                                <textarea class="form-control textarea" name="message" value="{{ old('message') }}" placeholder="About Job Seeker"></textarea>
			                                                @if($errors->has('message'))
			                                                    <span class="label label-danger">
			                                                        {{ $errors->first('message') }}
			                                                    </span>
			                                                @endif 
			                                            </div>
			                                            <div class="form-group">
			                                                <button type="Submit" class="btn btn-success">Submit</button>
			                                            </div>
			                                        </form>
		                                    	</div>
				                            </div>
				                            <div class="modal-footer">
				                            <button type="button" class="btn btn-primary modal_btn" data-dismiss="modal">Close</button>
				                            </div>
				                        </div>
				                    </div>
				                </div>

							</div>
							<div class="brows-job-link app_list_btn job_list_btn">

								<button type="button" class="btn btn-success sel_btn" data-toggle="modal" data-target="#status_two_modal_two">
				                  Rejected
				                </button>

				                <!-- Modal -->
				                <div class="modal fade" id="status_two_modal_two" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
				                    <div class="modal-dialog modal-dialog-centered" role="document">
				                        <div class="modal-content">
				                            <div class="modal-header">
				                            <h5 class="modal-title" id="exampleModalLongTitle">Send Notification</h5>
				                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				                              <span aria-hidden="true">&times;</span>
				                            </button>
				                            </div>
				                            <div class="modal-body">
												<div class="modal_job_inp">
													<div class="modal_job_summ">
				                                       <span class="mjs_con">
				                                           {{ $application_datas->seeker_first_name }}
				                                           {{ $application_datas->seeker_last_name }}
				                                       </span><br>
				                                       <span class="mjs_con">
				                                           {{ $application_datas->seeker_deg }}
				                                       </span>
				                                   </div><br>
			                                        <form action="{{route('reject_status_value', $application_datas->id)}}" role="" method="post">
			                                            {{csrf_field()}}
			                                            <div class="input-group form-group">
			                                                <label class="exp_level" for="exp_salary">Message</label><br>
			                                                <textarea class="form-control textarea" name="message" value="{{ old('message') }}" placeholder="About Job Seeker"></textarea>
			                                                @if($errors->has('message'))
			                                                    <span class="label label-danger">
			                                                        {{ $errors->first('message') }}
			                                                    </span>
			                                                @endif 
			                                            </div>
			                                            <div class="form-group">
			                                                <button type="Submit" class="btn btn-success">Submit</button>
			                                            </div>
			                                        </form>
		                                    	</div>
				                            </div>
				                            <div class="modal-footer">
				                            <button type="button" class="btn btn-primary modal_btn" data-dismiss="modal">Close</button>
				                            </div>
				                        </div>
				                    </div>
				                </div>

							</div>
							@endif
						</div>
					</div>
				</div>
			</article>
		</div>
		@endforeach
	</div>
</div>

@endsection