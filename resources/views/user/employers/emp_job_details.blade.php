@extends('user.employers.master')

@section('contant')
@foreach($emp_job_data as $emp_jobs)
<div class="row ptb_20 bb_2">
	<h2 class="detail-title">Job Details</h2>
	<div class="col-md-6 col-sm-6 form-group">
		<div class="input-group">
			<span class="titlefix">
				Job Title: 
			</span>
			<span class="title">
				{{$emp_jobs->job_title}}
			</span>
		</div>
		<div class="input-group">
			<span class="titlefix">
				Job Categorie: 
			</span>
			<span class="title">
				{{$emp_jobs->job_cate}}
			</span>
		</div>
		<div class="input-group">
			<span class="titlefix">
				Salary: 
			</span>
			<span class="title">
				{{$emp_jobs->salary}}
			</span>
		</div>
		<div class="input-group logo_fprm date_form">
			<span class="titlefix">
				DateLine: 
			</span>
			<span class="title">
				{{$emp_jobs->dateline}}
			</span>
		</div>	
	</div>
	
	<div class="col-md-6 col-sm-6 form-group">
		<div class="input-group">
			<span class="titlefix">
				No. of Vacancies: 
			</span>
			<span class="title">
				{{$emp_jobs->vacancies}}
			</span>
		</div>
		<div class="input-group">
			<span class="titlefix">
				Location: 
			</span>
			<span class="title">
				{{$emp_jobs->location}}
			</span>
		</div>
		<div class="input-group">
			<span class="titlefix">
				Job Level: 
			</span>
			<span class="title">
				{{$emp_jobs->job_level}}
			</span>
		</div>
	</div>
	<div class="">
		<span class="titlefix">
			Job Description: 
		</span>
		<span class="title">
			{{$emp_jobs->job_desc}}
		</span>
	</div>
</div>
<div class="row ptb_20">
	<h2 class="detail-title">Candidates Requirements</h2>
	<div class="col-md-6 col-sm-6 form-group">
		<div class="input-group">
			<span class="titlefix">
				Require Degree: 
			</span>
			<span class="title">
				{{$emp_jobs->candi_degree}}
			</span>
		</div>
		<div class="input-group">
			<span class="titlefix">
				Age: 
			</span>
			<span class="title">
				{{$emp_jobs->age}}
			</span>
		</div>
		<div class="input-group">
			<span class="titlefix">
				Additional Requirements: 
			</span>
			<span class="title">
				{{$emp_jobs->additional_req}}
			</span>
		</div>	
	</div>
	
	<div class="col-md-6 col-sm-6 form-group">
		<div class="input-group">
			<span class="titlefix">
				Experience: 
			</span>
			<span class="title">
				{{$emp_jobs->experience}}
			</span>
		</div>
		<div class="input-group">
			<span class="titlefix">
				Gender: 
			</span>
			<span class="title">
				{{$emp_jobs->gender}}
			</span>
		</div>
		<div class="input-group">
			<span class="titlefix">
				Compensation & Other Benefits: 
			</span>
			<span class="title">
				{{$emp_jobs->other_beni}}
			</span>
		</div>		
	</div>
</div>
<div class="row ptb_20">	
	
</div>
@endforeach
@endsection