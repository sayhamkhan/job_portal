@extends('user.master')

@section('contant')

@if (session()->has('message'))
    <div class="alert alert-success">
        {{ session('message') }}
    </div>
@endif
<form action="{{route('create_profile_process')}}" role="" method="post" enctype="multipart/form-data">
	@csrf
	<div class="row ptb_20 bb_2">
		<h2 class="detail-title">Upload Image</h2>
        <div class="form-group">
            <label for="exampleInputPhoto">Profile Photo</label>
            <input type="file" class="form-control" name="profile_photo">
            @if($errors->has('profile_photo'))
                <span class="label label-danger">
                    {{ $errors->first('profile_photo') }}
                </span>
            @endif
		</div>
		<h2 class="detail-title mt_50">Job Seeker Details</h2>
		<div class="col-md-6 col-sm-6 form-group">
			<div class="input-group">
				<h1>Name : </h1>
				<h1>{{ auth()->user()->first_name }}{{ auth()->user()->last_name }}</h1>
			</div>
			<div class="input-group">
				<h1>Phone : </h1>
				<h1>{{auth()->user()->phone}}</h1>
			</div>	
		</div>
		
		<div class="col-md-6 col-sm-6 form-group">
			<div class="input-group">
				<h1>Email : </h1>
				<h1>{{auth()->user()->email}}</h1>
			</div>	
		</div>
	</div>
	<div class="row ptb_20 bb_2">
		<h2 class="detail-title">Personal Information</h2>
		<div class="col-md-6 col-sm-6 form-group">
			<div class="input-group">
				<input type="text" pattern="[a-zA-Z]*" name="father_name" value="{{ old('father_name') }}" class="form-control" placeholder="Father Name">
				@if($errors->has('father_name'))
                    <span class="label label-danger">
                        {{ $errors->first('father_name') }}
                    </span>
                @endif
			</div>
			<div class="input-group">
				<span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
				<select name="curent_location" class="form-control input-lg">
					@foreach($location as $locations)
					<option>{{$locations->location_name}}</option>
					@endforeach
				</select>
				@if($errors->has('curent_location'))
                    <span class="label label-danger">
                        {{ $errors->first('curent_location') }}
                    </span>
                @endif
			</div>
			<div class="input-group">
				<input type="text" name="nationality" value="{{ old('nationality') }}" class="form-control" placeholder="Nationality">
				@if($errors->has('nationality'))
                    <span class="label label-danger">
                        {{ $errors->first('nationality') }}
                    </span>
                @endif
			</div>
			<div class="input-group">
				<select name="sek_categoory" class="form-control input-lg">
					@foreach($category as $categorys)
					<option>{{$categorys->cat_name}}</option>
					@endforeach
				</select>
				@if($errors->has('sek_categoory'))
                    <span class="label label-danger">
                        {{ $errors->first('sek_categoory') }}
                    </span>
                @endif
			</div>
			<div class="input-group mari_stu">
				<input name="marital_status" type="radio" class="form-control" name="gender" value="Maried">
				<span class="g_name">Maried</span> 
				<input name="marital_status" type="radio" class="form-control" name="gender" value="Unmaried"> 
				<span class="g_name">Unmaried</span>
			</div>	
		</div>
		
		<div class="col-md-6 col-sm-6 form-group">
			<div class="input-group">
				<input type="text" class="form-control" name="mother_name" value="{{ old('mother_name') }}" placeholder="Mother Name">
				@if($errors->has('mother_name'))
                    <span class="label label-danger">
                        {{ $errors->first('mother_name') }}
                    </span>
                @endif
			</div>
			<div class="input-group">
				<span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
				<input type="text" name="address" value="{{ old('address') }}" class="form-control" placeholder="Address">
				@if($errors->has('address'))
                    <span class="label label-danger">
                        {{ $errors->first('address') }}
                    </span>
                @endif
			</div>
			
			<div class="input-group input-append date">
	            <input type="text" class="form-control" name="birth_date" value="{{ old('birth_date') }}" placeholder="Date of Birth" />
	            @if($errors->has('birth_date'))
                    <span class="label label-danger">
                        {{ $errors->first('birth_date') }}
                    </span>
                @endif
	        </div>
	        <div class="input-group">
				<input type="text" class="form-control" name="sek_deg" value="{{ old('sek_deg') }}" placeholder="Seeker Degignation">
				@if($errors->has('sek_deg'))
                    <span class="label label-danger">
                        {{ $errors->first('sek_deg') }}
                    </span>
                @endif
			</div>
			<div class="input-group">
				<input name="gender" type="radio" class="form-control" name="gender" value="Male">
				<span class="g_name">Male</span> 
				<input name="gender" type="radio" class="form-control" name="gender" value="Female"> 
				<span class="g_name">Female</span> 
				<input name="gender" type="radio" class="form-control" name="gender" value="Other"> 
				<span class="g_name">Other</span> 
			</div>
				
		</div>
	</div>
	<div class="row ptb_20">
		<h2 class="detail-title">About Job Seeker</h2>
		
		<div class="col-md-12 col-sm-12">
			<textarea class="form-control textarea" name="sek_dec" value="{{ old('sek_dec') }}" placeholder="About Job Seeker"></textarea>
			@if($errors->has('sek_dec'))
                <span class="label label-danger">
                    {{ $errors->first('sek_dec') }}
                </span>
            @endif
		</div>	
		<div class="col-md-12 col-sm-12 mt_10">
			<button class="btn btn-success btn-primary mid_btn">Submit</button>	
		</div>	
	</div>
</form>
@endsection