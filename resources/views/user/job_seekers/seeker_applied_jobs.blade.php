@extends('user.master')

@section('contant')

<div class="row">
	<div class="col-md-12">
		@foreach($seeker_application_data as $seeker_application_datas)
		<div class="item-click">
			<article>
				<div class="brows-job-list">
					<div class="row">
						<div class="col-md-4 col-sm-6">
							<div class="brows-job-position">
								<a href="#">
									<h3>
										{{ $seeker_application_datas->job_title }}
									</h3>
								</a>
								<p>
									<span>Vacancy: {{ $seeker_application_datas->vacacy }}</span>
								</p>
							</div>
						</div>
						<div class="col-md-2 col-sm-2">
							<div class="brows-job-location">
								<h3>
									{{ $seeker_application_datas->cmp_name }}
								</h3>
								<p>{{ $seeker_application_datas->salary }}</p>
							</div>
						</div>
						<div class="col-md-2 col-sm-2">
							<div class="brows-job-location">
								<p>{{ $seeker_application_datas->cmp_address }}</p>
							</div>
						</div>
						<div class="col-md-4 col-sm-2">
							@if($seeker_application_datas->status==0)
							<div class="brows-job-link job_list_btn">
								<a href="#" class="btn btn-info">Pending</a>
							</div>
							@elseif($seeker_application_datas->status==1)
							<div class="brows-job-link job_list_btn">
								<a href="#" class="btn btn-success">Selected</a>
							</div>
							@else
							<div class="brows-job-link job_list_btn">
								<a href="#" class="btn btn-success sel_btn">Rejected</a>
							</div>
							@endif
							<div class="brows-job-link job_list_btn">
								<a href="{{route('emp_datils_by_application', $seeker_application_datas->employer_id)}}" class="btn btn-success">Details</a>
							</div>
						</div>
					</div>
				</div>
			</article>
		</div>
		@endforeach
	</div>
</div>

@endsection