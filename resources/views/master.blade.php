<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Job Portal</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->


    <!--For Owl Carousel-->
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{ asset('css/normalize.css')}}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('css/all.css')}}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{ asset('css/datatables.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/main.css')}}">
    <link rel="stylesheet" href="{{ asset('css/responsive.css')}}">
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>

	<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
    <!--===========Header Start Here===========-->
    @include('partials.header')
    <!--===========Header End Here===========-->
    <!--===========Slider Start Here===========-->

    @yield('slider')

    <!--=============Slider End Here=============-->
    <!--=============Quick_Job_Item Start Here=============-->
    @yield('quick_job')
    <!--=============Quick_Job_Item End Here=============-->
    <!--=============Page_Contant Start Here=============-->
    @yield('contant')
    <!--=============Page_Contant End Here=============-->
    <!--=============Footer Widgets Start Here=============-->
    @yield('footer_widgets')
    <!--=============Footer Widgets End Here=============-->
    <!--=============Footer Start Here=============-->
    @include('partials.footer')
    <!--=============Footer End Here=============-->














    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
    <script src="{{ asset('js/plugins.js') }}"></script>
    <script src="{{ asset('js/datatables.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('js/bootstrap.js') }}"></script>
    
    <script>
        $(document).ready(function() {
            $('#dataTables').DataTable();
        } );
    </script>

    <!--For Owl Carousel-->
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <!-- For Sticky Menu -->
    <script src="{{ asset('js/jquery.sticky.js') }}"></script>
    <!-- For Scroll Up -->
    <script src="{{ asset('js/jquery.scrollUp.js') }}"></script>
    <!-- Font Awesome -->
    <script src="{{ asset('js/all.js') }}"></script>
    <!--For Bootstrap Editor-->
    <script type="text/javascript" src="{{ asset('js/wysihtml5-0.3.0.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-wysihtml5.js') }}"></script>
    <!--For DatePicker-->
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
    <!--For Counter Up-->
    <script src="{{ asset('js/counterup.min.js') }}"></script>
    <script src="{{ asset('js/waypoints.min.js') }}"></script>
    

</body>
</html>

