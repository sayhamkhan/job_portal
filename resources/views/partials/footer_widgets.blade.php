    <!-- Top Footer -->
    <div class="top_footer bg_2">
        <!-- Container -->
        <div class="container">
            <!-- Row -->
            <div class="row">
            
                <!-- Widget About -->
                <div class="col-md-4 col-sm-6">
                    <aside class="widget widget_about">
                        <h3 class="widget-title">ABOUT US</h3>
                        <hr>
                        <p>career.com is an online employment solution for people seeking jobs and the employers who need great people in Bangladesh. Our aim is to help the people find jobs and connect them with the leading companies in the country. Chakri.com is the top most transparent career community in Bangladesh that is changing the way people find jobs, and companies recruit top talent. We help to connect job seekers directly with ...</p>
                    </aside>
                </div>
                <!-- Widget About /- -->
                <!-- Widget Job Seekers -->
                <div class="col-md-3 col-sm-6">
                    <aside class="widget widget_links">
                        <h3 class="widget-title">Job Seekers</h3>
                        <hr>
                        <ul>
                            <li><a title="Home" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>
                            Create Account</a></li>
                            <li><a title="Menu" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>
                            Career Counseling</a></li>
                            <li><a title="About" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>
                            My Bdjobs</a></li>
                            <li><a title="Blog" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>
                            FAQ</a></li>
                            <li><a title="Contact" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>
                            Guides</a></li>
                        </ul>
                    </aside>
                </div>
                <!-- Widget Job Seekers /- -->
                
                <!-- Widget Working Time -->
                <div class="col-md-3 col-sm-6">
                    <aside class="widget widget_workingtime">
                        <h3 class="widget-title">Employers</h3>
                        <hr>
                        <div class="working-time-table">
                            <ul>
                                <li><a title="Home" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                Create Account</a></li>
                                <li><a title="Menu" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                Products/Service</a></li>
                                <li><a title="About" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                Post a Job</a></li>
                                <li><a title="Blog" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                FAQ</a></li>
                            </ul>
                        </div>
                    </aside>
                </div>
                <!-- Widget Working Time -->
                <!-- Widget Category -->
                <div class="col-md-2 col-sm-6">
                    <aside class="widget widget_links">
                        <h3 class="widget-title">Follow</h3>
                        <hr>
                        <ul class="widget widget_social">
                            <li><a title="Facebook" href="#"><i class="fa fa-facebook-f"></i></a></li>
                            <li><a title="Twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a title="Linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a title="Pinterest" href="#"><i class="fa fa-youtube"></i></a></li>
                        </ul>
                    </aside>                    
                </div><!-- Widget Category /- -->
            </div><!-- Row /- -->
        </div><!-- Container /- -->
    </div><!-- Top Footer -->