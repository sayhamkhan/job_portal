<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class traning_summary extends Model
{
    protected $fillable = [
        'seeker_id', 'title','institute', 'location', 'duration', 'topic', 'country', 'year', 
    ];
}
