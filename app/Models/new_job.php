<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class new_job extends Model
{
    protected $fillable = [
        'employer_id', 'job_title','job_desc', 'job_cate', 'salary', 'dateline', 'vacancies', 'location', 'job_level', 'candi_degree', 'age', 'gender', 'experience', 'additional_req', 'other_beni', 'status',
    ];
}
