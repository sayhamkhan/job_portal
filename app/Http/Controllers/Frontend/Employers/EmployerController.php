<?php

namespace App\Http\Controllers\Frontend\Employers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\employer_profile;
use App\Models\new_job;
use App\Models\Applied_Jobs;
use App\Models\seeker_profile;
use App\Models\Notification;
use App\Models\Category;
use App\Models\Location;
use App\Models\Academic_Qualification_Admin;

class EmployerController extends Controller
{
    public function employer_dashbord(){

        $status=0;
        $notification_data= Notification::where('sekker_id', auth()->user()->id)->where('status',$status)->get();
        $notification_number=$notification_data->count();

        $user_id=(auth()->user()->id);
        $emp_data= new_job::where('employer_id', $user_id)->get();
        $job_count=$emp_data->count();

        $application_count= Applied_Jobs::where('employer_id', $user_id)->get();
        $app_count=$application_count->count();
        
    	return view('user.employers.employer_dashbord', compact('job_count', 'notification_number', 'app_count'));
    }

    public function emp_notification_details($id){

        $input=1;

        $notification=Notification::where('id',$id)->first();

        Notification::where('id', $id)->Update([
            
            'status'=>$input,

        ]);

        return view('user.employers.emp_notification_details', compact('notification'));
    }

    public function employer_profile(){
        
    	return view('user.employers.employers_profile');
    }

    public function employer_profile_view(){

        $emp_data= employer_profile::where('user_id',auth()->user()->id)->first();
    	return view('user.employers.employers_profile_view', compact('emp_data'));
    }

    public function new_job(){

        $category= Category::all();
        $location= Location::all();
        $qualification= Academic_Qualification_Admin::all();

    	return view('user.employers.create_job', compact('category', 'location', 'qualification'));
    }

    public function edit_job(){

        $emp_job_data=new_job::where('employer_id',auth()->user()->id)->get();
        return view('user.employers.edit_job', compact('emp_job_data'));
    }

    public function edit_job_details($id){

        $emp_job_data=new_job::where('employer_id',auth()->user()->id)->where('id',$id)->get();
        return view('user.employers.edit_job_details', compact('emp_job_data'));
    }

    public function emp_job_details($id){
        
        $emp_job_data=new_job::where('employer_id',auth()->user()->id)->where('id',$id)->get();
        return view('user.employers.emp_job_details', compact('emp_job_data'));
    }

    public function application_list($id){
        

        $application_data=Applied_Jobs::where('job_id',$id)->get();
        /*$application_seeker=Applied_Jobs::all()->where('job_id',$id)->first();
        $application_seeker_id= ($application_seeker->sekker_id);
        $application_seeker_data=seeker_profile::all()->where('user_id', $application_seeker_id)->first();*/
        //dd($application_seeker_data);
        return view('user.employers.application_list', compact('application_data'));
    }

    public function seeker_datils_by_job($sekker_id){
        

        $data=seeker_profile::where('user_id',$sekker_id)->first();
        /*$application_seeker=Applied_Jobs::all()->where('job_id',$id)->first();
        $application_seeker_id= ($application_seeker->sekker_id);
        $application_seeker_data=seeker_profile::all()->where('user_id', $application_seeker_id)->first();*/
        //dd($data);
    	return view('user.employers.seeker_datils_by_job', compact('data'));
    }

    public function employer_profile_process(Request $request)
    {
        //Get All Input Data

        $inputs = $request->except('_token');

        $validator = Validator::make($inputs, [

             'cmp_name' => 'required',
             'cmp_tagline' => 'required',
             'cmp_categoory' => 'required',
             'cmp_city' => 'required',
             'cnt_designation' => 'required',
             'cmp_website' => 'required',
             'cmp_address' => 'required',
             'employe_rang' => 'required',
             'cmp_dec' => 'required',
             'cmp_logo' => 'required|image',
        ]);

         if ($validator->fails()) {
             return redirect()->back()->withErrors($validator)->withInput();
         }




        // save the file
        $profile_photo = $request->file('cmp_logo');

        $file_name=uniqid("photo_",true).str_random(10).'.'.$profile_photo->getClientOriginalExtension();
         // dd($file_name);
            if($profile_photo->isValid()){
               
                $profile_photo->storeAs('img',$file_name);
            }
        //$cmp_logo = $request->file('cmp_logo');
        //dd($cmp_logo);
        //$path = $cmp_logo->store('cmp_logo');


        

        $auth_id= auth()->user()->id;
//dd($auth_id);
        $test=employer_profile::where('user_id', $auth_id )->Update([
            'cmp_name'=>trim($request->input('cmp_name')),
            'cmp_tagline' => trim($request->input('cmp_tagline')),
            'cmp_categoory' => trim($request->input('cmp_categoory')),
            'cmp_city' => trim($request->input('cmp_city')),
            'cnt_designation' => trim($request->input('cnt_designation')),
            'cmp_website' => trim($request->input('cmp_website')),
            'cmp_address' => trim($request->input('cmp_address')),
            'employe_rang' => trim($request->input('employe_rang')),
            'cmp_dec' => trim($request->input('cmp_dec')),
            'cmp_logo' => $file_name,

        ]);

        session()->flash('message', 'Profile Update successfully.');
        return redirect()->route('employer_profile_view');


    }

    public function create_job_process(Request $request)
    {
        //Get All Input Data

        $inputs = $request->except('_token');

        $validator = Validator::make($inputs, [

             'job_title' => 'required',
             'job_desc' => 'required',
             'job_cate' => 'required',
             'salary' => 'required',
             'dateline' => 'required',
             'vacancies' => 'required',
             'location' => 'required',
             'job_level' => 'required',
             'candi_degree' => 'required',
             'age' => 'required',
             'gender' => 'required',
             'experience' => 'required',
             'additional_req' => 'required',
             'other_beni' => 'required',
        ]);

         if ($validator->fails()) {
             return redirect()->back()->withErrors($validator)->withInput();
         }

        $employers= employer_profile::all()->where('user_id', auth()->user()->id)->first();

        new_job::create([
            'employer_id'=>($employers->user_id),
            'job_title'=>trim($request->input('job_title')),
            'job_desc' => trim($request->input('job_desc')),
            'job_cate' => trim($request->input('job_cate')),
            'salary' => trim($request->input('salary')),
            'dateline' => trim($request->input('dateline')),
            'vacancies' => trim($request->input('vacancies')),
            'location' => trim($request->input('location')),
            'job_level' => trim($request->input('job_level')),
            'candi_degree' => trim($request->input('candi_degree')),
            'age' => trim($request->input('age')),
            'gender' => trim($request->input('gender')),
            'experience' => trim($request->input('experience')),
            'additional_req' => trim($request->input('additional_req')),
            'other_beni' => trim($request->input('other_beni')),

        ]);
        //dd($employers);

        session()->flash('message', 'Accademic Qualification added successfully.');
        return redirect()->route('emp_job_list');
    }

    public function emp_job_list()
    {
        
        $emp_job_data=new_job::where('employer_id',auth()->user()->id)->get();
        //dd($emp_job_data);
        return view('user.employers.emp_job_list', compact('emp_job_data'));
    }
}
