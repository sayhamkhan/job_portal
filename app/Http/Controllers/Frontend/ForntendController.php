<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;
use App\Models\new_job;
use App\Models\Applied_Jobs;
use App\Models\seeker_profile;
use App\Models\employer_profile;
use App\Models\Notification;
use App\Models\User;
use App\Models\Location;
use App\Models\Category;

class ForntendController extends Controller
{
    public function index(){
        $location= Location::all();
        $category= Category::all();
        $category_find= Category::select(['cat_name'])->get();
        $job_salary=20000;
        $feature_job= new_job::where('salary','>=', $job_salary)->get();
        //dd($feature_job);
        return view('index', compact('location', 'category', 'feature_job'));
    }

    public function contact(){
    	return view('contact');
    }

    public function create(){
    	return view('user.registration');
    }

    public function login(){
    	return view('user.login');
    }

    public function jobs()
    {
        $emp_job_data=new_job::all();
        $user=User::select(['user_type'])->get();
        //dd($user);
        return view('jobs', compact('emp_job_data'));
    }

    public function job_details($id)
    {
        $user_id=(auth()->user()->id);
        $job_data=new_job::where('id',$id)->get();
        $check_apply=Applied_Jobs::where('sekker_id', $user_id)->where('job_id', $id)->first();
        //dd($check_apply);
        return view('job_details', compact('job_data', 'check_apply'));
    }

    public function job_applied($id, Request $request)
    {
        $job_id=$id;
        $seeker_id=(auth()->user()->id);
        $seeker_first_name=(auth()->user()->first_name);
        $seeker_last_name=(auth()->user()->last_name);
        $seeker= seeker_profile::all()->where('user_id',$seeker_id)->first();
        $seeker_deg=($seeker->sek_deg);
        $seeker_gender=($seeker->gender);
        $job= new_job::all()->where('id',$job_id)->first();
        $job_title=($job->job_title);
        $job_salary=($job->salary);
        $job_vacancy=($job->vacancies);
        $job_location=($job->location);
        $employer= new_job::all()->where('id',$id)->first();
        $employer_id=($employer->employer_id);
        $employer_details= employer_profile::all()->where('id',$employer_id)->first();
        $comany_name=($employer_details->cmp_name);
        $comany_address=($employer_details->cmp_address);

        $inputs = $request->except('_token');
        // $validator = Validator::make($inputs, [

        //      'exp_salary' => 'required',
        // ]);

        //  if ($validator->fails()) {
        //      return redirect()->back()->withErrors($validator)->withInput();
        //  }

       $test= Applied_Jobs::create([
            'job_id'=>$job_id,
            'employer_id'=>($employer->employer_id),
            'sekker_id' => $seeker_id,
            'seeker_first_name' => $seeker_first_name,
            'seeker_last_name' => $seeker_last_name,
            'seeker_deg' => $seeker_deg,
            'seeker_gender' => $seeker_gender,
            'job_title' => $job_title,
            'salary' => $job_salary,
            'vacacy' => $job_vacancy,
            'location' => $job_location,
            'cmp_name' => $comany_name,
            'cmp_address' => $comany_address,
            'exp_salary' => trim($request->input('exp_salary')),

        ]);

       Notification::create([
            
            'job_id'=>$job_id,
            'employer_id'=>($employer->employer_id),
            'job_title'=>$job_title,
            'first_name'=>$seeker_first_name,
            'last_name'=>$seeker_last_name,
            'sec_deg'=>$seeker_deg,
            'exp_salary'=>trim($request->input('exp_salary')),

        ]);

        //dd($test);

        session()->flash('message', 'Accademic Qualification added successfully.');
        return redirect()->route('jobs');
        //dd($employer);

    }

    public function select_status_value($id, Request $request)
    {
        $input=1;

        $application_data= Applied_Jobs::all()->where('id',$id)->first();

        $application_job_id=($application_data->job_id);

        $application_employer_id=($application_data->employer_id);

        $application_sekker_id=($application_data->sekker_id);

        $application_job_title=($application_data->job_title);

        $application_salary=($application_data->salary);

        $message=trim($request->input('message'));

        Applied_Jobs::where('id', $id)->Update([
            
            'status'=>$input,

        ]);

        Notification::create([
            
            'job_id'=>$application_job_id,
            'sekker_id'=>$application_sekker_id,
            'job_title'=>$application_job_title,
            'salary'=>$application_salary,
            'message'=>$message,

        ]);
//dd($test);
                session()->flash('message', 'Update successfully.');
                return redirect()->back();
        
    }

    public function reject_status_value($id, Request $request)
    {
        
        $input=2;

        $application_data= Applied_Jobs::all()->where('id',$id)->first();

        $application_job_id=($application_data->job_id);

        $application_employer_id=($application_data->employer_id);

        $application_sekker_id=($application_data->sekker_id);

        $application_job_title=($application_data->job_title);

        $application_salary=($application_data->salary);

        $message=trim($request->input('message'));

        Applied_Jobs::where('id', $id)->Update([
            
            'status'=>$input,

        ]);

        Notification::create([
            
            'job_id'=>$application_job_id,
            'sekker_id'=>$application_sekker_id,
            'job_title'=>$application_job_title,
            'salary'=>$application_salary,
            'message'=>$message,

        ]);
//dd($test);
        session()->flash('message', 'Update successfully.');
        return redirect()->back();
        
    }


}
